require('dotenv').config();
const bot = require('./helpers/bot.helper');
const { time } = require('./helpers/utils.helper');
const tasker = require('./scheduler');

const txt =
  '========= JARADA =========\n' +
  `Time start: ${time().format('DD/MM/YYYY HH:mm:ss')} WIB\n` +
  '==========================';
console.log(txt);

require('./commands');
bot.launch();
tasker();
console.log('==========================\n');
