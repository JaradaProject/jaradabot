const { get } = require('lodash');
const { telegram } = require('../../helpers/bot.helper');
const errorHandler = require('../../helpers/error.helper');
const { ingressStatHelper } = require('../../helpers/ingressStat.helper');
const { getCommandString, getArgument, userCaller } = require('../../helpers/utils.helper');

module.exports = {
  manualStat: {
    description: 'Convert Stat Manually',
    group: false,
    commandFrom: 'text',
    middlewares: ['owner'],
    addToMyCommand: false,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      //** check format command */
      const { from, text } = get(ctx, 'update.message');
      const comm = getCommandString(text).command;
      const argument = await getArgument(comm, text).catch(() => {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: from.id,
            name: 'Masalah menggunakan command',
            message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
            detail: `Format submit stat seharusnya:\n${getCommandString(text).removeBotName()}<spasi>STATS INGRESS PRIME`,
            stack: null,
          },
        });
      });

      const stats = await ingressStatHelper(
        argument
          .split('\n')
          .map((t) => t.replace(/\s\s/g, ' '))
          .join('\n')
      );
      telegram.sendMessage(from.id, stats.formated, { parse_mode: 'Markdown' });
    },
  },
};
