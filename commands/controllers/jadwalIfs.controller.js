const { get } = require('lodash');
const db = require('../../databases/models');
const { telegram } = require('../../helpers/bot.helper');
const { userCaller, envConfig, time } = require('../../helpers/utils.helper');

module.exports = {
  jadwalIfs: {
    description: 'Jadwal IFS Yang sedang berlangsung',
    group: 'true',
    commandFrom: 'text',
    middlewares: ['date'],
    addToMyCommand: true,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      const dateConfs = await db.dateConfs.findAll({ raw: true });
      const getCommDate = (comm, typeDate) => {
        const commDate = dateConfs.find((obj) => obj.comm == comm);
        return time(commDate[typeDate]).format('HH:mm');
      };

      const msg =
        'Ini jadwal IFS hari ini' +
        '\n\n' +
        '💠️ Submit Stat Awal pukul:\n' +
        `👉️ *${getCommDate('/statawal', 'minDate')} - ${getCommDate('/statawal', 'maxDate')} WIB*` +
        '\n\n' +
        '💠️ Zoom Aktif pukul:\n' +
        `👉️ *${getCommDate('/linkzoom', 'minDate')} - ${getCommDate('/linkzoom', 'maxDate')} WIB*` +
        '\n\n' +
        '💠️ Sesi foto zoom pukul:\n' +
        `👉️ *Sesi 1: ${getCommDate('zoomSession1', 'minDate')} - ${getCommDate('zoomSession1', 'maxDate')} WIB*\n` +
        `👉️ *Sesi 2: ${getCommDate('zoomSession2', 'minDate')} - ${getCommDate('zoomSession2', 'maxDate')} WIB*\n` +
        '\n\n' +
        '💠️ Submit Stat Akhir pukul:\n' +
        `👉️ *${getCommDate('/statakhir', 'minDate')} - ${getCommDate('/statakhir', 'maxDate')} WIB*`;

      telegram.sendMessage(envConfig('GROUP_ID'), `${userCaller(get(ctx, 'update.message.from'))}\n${msg}`, {
        parse_mode: 'Markdown',
      });
      return;
    },
  },
};
