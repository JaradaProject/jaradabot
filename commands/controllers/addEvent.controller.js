const db = require('../../databases/models');
const { get } = require('lodash');
const { telegram } = require('../../helpers/bot.helper');
const errorHandler = require('../../helpers/error.helper');
const { envConfig, userCaller, getCommandString, getArgument, deleteMessage } = require('../../helpers/utils.helper');
const axios = require('axios');
const cheerio = require('cheerio');

module.exports = {
  addEvent: {
    description: 'Add new event IFS',
    group: true,
    commandFrom: 'text',
    middlewares: ['owner'],
    addToMyCommand: true,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      const { from, text, message_id } = get(ctx, 'update.message');
      const comm = getCommandString(text).command;
      const argument = await getArgument(comm, text).catch(() => {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: from.id,
            name: 'Masalah menggunakan command',
            message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
            detail: `Format submit event seharusnya:\n${getCommandString(text).removeBotName()}<spasi>EVENT ID`,
            stack: null,
          },
        });
      });

      deleteMessage(ctx, message_id);

      const html = await axios({
        method: 'get',
        url: `https://fevgames.net/ifs/event/?e=${argument}`,
      });

      const $ = cheerio.load(html.data);
      const name = $('h2').text();
      if (name == '#IngressFS - ') {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: from.id,
            name: 'Masalah menggunakan command',
            message: `Hai, ${userCaller(from)} Command gagal digunakan`,
            detail: `Event dengan ID yang dimaksud tidak ditemukan silahkan periksa link event yang dikirim oleh fevgame melalui email poc`,
            stack: null,
          },
        });
      }
      await db.events.create({ id: argument, name }).catch((error) => {
        if (error.name == 'SequelizeUniqueConstraintError') {
          errorHandler(null, {
            logger: null,
            rawOption: null,
            data: {
              id: from.id,
              name: `Masalah menggunakan command ${getCommandString(text).removeBotName()}`,
              message: `Hai, ${userCaller(from)} Command gagal digunakan`,
              detail: `Gagal menambahkan event ke database, karena sudah didaftarkan sebelumnya`,
              stack: null,
            },
          });
        } else {
          errorHandler(error, {
            logger: 'Create event fail',
            rawOption: true,
            data: {
              id: from.id,
              name: `Masalah menggunakan command ${getCommandString(text).removeBotName()}`,
              message: `Hai, ${userCaller(from)} Command gagal digunakan`,
              detail: `Gagal menambahkan event ke database, silahkan cek logger`,
              stack: null,
            },
          });
        }
      });
      await db.events.update({ status: 'inactive' }, { where: { status: 'active' } });
      await db.events.update({ status: 'active' }, { where: { id: argument } });
      telegram.sendMessage(envConfig('GROUP_ID'), `Berhasil menambahkan event ${name}`);
    },
  },
};
