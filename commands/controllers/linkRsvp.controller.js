const { get } = require('lodash');
const errorHandler = require('../../helpers/error.helper');
const { userCaller, envConfig } = require('../../helpers/utils.helper');
const { telegram } = require('../../helpers/bot.helper');
const db = require('../../databases/models');

module.exports = {
  linkRsvp: {
    description: 'Link zoom IFS hari ini',
    group: true,
    commandFrom: 'text',
    middlewares: [],
    addToMyCommand: true,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      const result = await db.events
        .findOne({ raw: true, where: { status: 'active' }, attributes: ['id'] })
        .catch((error) => {
          errorHandler(error, {
            logger: 'Link RSVP Get Event Error DB',
            rawOption: true,
            data: {
              id: null,
              name: 'Masalah mengambil active event',
              message: null,
              detail: 'link rsvp controller',
              stack: true,
            },
          });
        });
      if (result == null) {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah menggunakan command',
            message: `Hai, ${userCaller(get(ctx, 'update.message.from'))} Ada masalah`,
            detail: 'PIC XFACT ID Belum mendaftarkan event ke Fevgames',
            stack: null,
          },
        });
      } else if (result.id) {
        const eventUrl = `${envConfig('FEVGAMES_EVENT_URL')}${result.id}`;
        const msg = `Ini link rsvpnya:\n👉️ [KLIK DI SINI](${eventUrl})\n\n⚠️ Pastikan ID kamu benar saat RSVP`;
        telegram.sendMessage(envConfig('GROUP_ID'), `${userCaller(get(ctx, 'update.message.from'))}\n${msg}`, {
          parse_mode: 'Markdown',
          disable_web_page_preview: true,
        });
      }
    },
  },
};
