const { get } = require('lodash');
const { telegram } = require('../../helpers/bot.helper');
const { deleteMessage } = require('../../helpers/utils.helper');

module.exports = {
  getGroupId: {
    description: 'Untuk mendapatkan id group',
    group: true,
    commandFrom: 'text',
    middlewares: ['owner'],
    addToMyCommand: false,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      const { id, title } = get(ctx, 'update.message.chat');
      telegram.sendMessage(get(ctx, 'update.message.from.id'), `${title} id: ${id}`);
      deleteMessage(ctx, get(ctx, 'update.message.message_id'));
      return;
    },
  },
};
