const { get } = require('lodash');
const db = require('../../databases/models');

module.exports = {
  myStat: {
    description: 'Add new event IFS',
    group: true,
    commandFrom: 'text',
    middlewares: ['date'],
    addToMyCommand: false,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      const { from } = get(ctx, 'update.message');
      const event = await db.events.findOne({ where: { status: 'active' } });
      await db.rsvps.findOne({ where: { userId: from.id, eventId: event.id } });
      return;
    },
  },
};
