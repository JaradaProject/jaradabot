const db = require('../../databases/models');
const { get } = require('lodash');
const { telegram } = require('../../helpers/bot.helper');
const errorHandler = require('../../helpers/error.helper');
const { ingressStatHelper } = require('../../helpers/ingressStat.helper');
const {
  envConfig,
  userCaller,
  time,
  getCommandString,
  getArgument,
  deleteMessage,
  numberFormat,
} = require('../../helpers/utils.helper');

module.exports = {
  statAkhir: {
    description: 'Submit stat akhir event IFS',
    group: true,
    commandFrom: 'text',
    middlewares: ['date'],
    addToMyCommand: true,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      const { from, text, message_id } = get(ctx, 'update.message');
      const comm = getCommandString(text).command;
      const argument = await getArgument(comm, text).catch(() => {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah menggunakan command',
            message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
            detail: `Format submit stat seharusnya:\n${getCommandString(text).removeBotName()}<spasi>STATS INGRESS PRIME`,
            stack: null,
          },
        });
      });

      const stats = await ingressStatHelper(
        argument
          .split('\n')
          .map((t) => t.replace(/\s\s/g, ' '))
          .join('\n')
      );
      deleteMessage(ctx, message_id);

      let user = await db.users.findOne({
        raw: true,
        where: { id: from.id },
      });

      if (user == null) {
        await db.users
          .create({
            id: from.id,
            name: stats.json['Agent Name'],
            faction: stats.json['Agent Faction'],
          })
          .then((result) => {
            user = result.get();
          });
      }
      if (user.name != stats.json['Agent Name']) {
        errorHandler(null, {
          logger: `Agent ${user.name} CN => stats.json['Agent Name']`,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah melakukan stat akhir',
            message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
            detail:
              `ID yang tersimpan ${user.name}, kamu mensubmit data agent ${stats.json['Agent Name']}\n` +
              `Apakah kamu melakukan ganti ID Sebelum IFS? Bila iya minta @SkyJackerz merubah ID kamu.\n` +
              `Bila tidak? Jangan mensubmit data stats milik orang lain.`,
            stack: null,
          },
        });
      }

      const statDateTime = `${stats.json['Date (yyyy-mm-dd)']} ${stats.json['Time (hh:mm:ss)']}`;
      const commDateTime = await db.dateConfs.findOne({
        raw: true,
        where: { comm: getCommandString(comm).removeBotName() },
        attributes: ['minDate', 'maxDate'],
      });

      if (time(statDateTime).isBefore(time(commDateTime.minDate))) {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah melakukan stat akhir',
            message: `Hai, ${userCaller(from)} Ada masalah penggunaan command`,
            detail: `Stat yang kamu copy merupakan stat lama atau dicopy sebelum pukul ${time(commDateTime.minDate).format(
              'HH:mm:ss DD/MM/YYYY'
            )} WIB\nSilahkan copy stat Ingress terbaru kamu`,
            stack: null,
          },
        });
      }

      const activeEvent = await db.events.findOne({
        raw: true,
        where: { status: 'active' },
        attributes: ['id'],
      });

      let isRsvp = await db.rsvps.findOne({
        raw: true,
        where: { userId: user.id, eventId: activeEvent.id },
      });

      if (isRsvp == null || Object.keys(isRsvp.startStat).length == 0) {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah melakukan stat akhir',
            message: `Hai, ${userCaller(from)} Ada masalah penggunaan command`,
            detail: 'Kamu belum submit stat awal, silahkan submit stat awal terlebih dahulu',
            stack: null,
          },
        });
      }

      if (Object.keys(isRsvp.endStat).length > 0 && isRsvp.endStat == stats.json) {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah melakukan stat akhir',
            message: `Hai, ${userCaller(from)} Ada masalah penggunaan command`,
            detail: `Kamu sudah submit stat akhir dengan poin yang sama pada ${time(isRsvp.updatedAt).format(
              'HH:mm:ss DD/MM/YYYY'
            )} WIB, silahkan action dan mengulangnya lagi`,
            stack: null,
          },
        });
      }
      await db.rsvps.update({ endStat: stats.json }, { where: { id: isRsvp.id } });

      const statChange = {
        Level: numberFormat.format(stats.json['Level'] - isRsvp.startStat['Level']),
        'Lifetime AP': numberFormat.format(stats.json['Lifetime AP'] - isRsvp.startStat['Lifetime AP']),
        'XM Recharged': numberFormat.format(stats.json['XM Recharged'] - isRsvp.startStat['XM Recharged']),
      };

      const msg =
        `👋 Hai, ${userCaller(from)}\n📝 Stat Akhir berhasil disimpan\n\n` +
        `✅ Agent Name: ${stats.json['Agent Name']}\n` +
        `✅ Level: ${stats.json['Level']} (+ ${statChange['Level']})\n` +
        `✅ Lifetime AP: ${numberFormat.format(stats.json['Lifetime AP'])} (+ ${statChange['Lifetime AP']})\n` +
        `✅ XM Recharged: ${numberFormat.format(stats.json['XM Recharged'])} (+ ${statChange['XM Recharged']})\n\n` +
        'Jangan lupa ikut sesi zoom 😁👍';

      telegram.sendMessage(envConfig('GROUP_ID'), msg, { parse_mode: 'Markdown' });
    },
  },
};
