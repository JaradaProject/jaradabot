const axios = require('axios');
const { get } = require('lodash');
const errorHandler = require('../../helpers/error.helper');
const { userCaller, envConfig, getArgument, getCommandString } = require('../../helpers/utils.helper');
const { telegram } = require('../../helpers/bot.helper');

module.exports = {
  updateZoom: {
    description: 'Link zoom IFS hari ini',
    group: false,
    commandFrom: 'text',
    middlewares: ['owner'],
    addToMyCommand: false,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      const { from, text } = get(ctx, 'update.message');
      const comm = getCommandString(text).command;
      const argument = await getArgument(comm, text).catch(() => {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah menggunakan command',
            message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
            detail: `Format submit stat seharusnya:\n${getCommandString(text).removeBotName()}<spasi>STATS INGRESS PRIME`,
            stack: null,
          },
        });
      });

      const gscriptUrl = envConfig('GSCRIPT_URL');
      const nodeEnv = envConfig('NODE_ENV');

      let response = await axios({
        url: gscriptUrl + `?mode=${nodeEnv}&script=updateZoomLink`,
        method: 'post',
        data: { link: argument },
      });
      response = get(response, 'data.response');

      if (parseInt(response.code) != 200) {
        errorHandler(response.result, {
          logger: `Failed update link zoom, with error ${response.result}`,
          rawOption: false,
          data: {
            id: from.id,
            name: 'Masalah saat mencoba menyimpan link zoom',
            message: `Hai, ${userCaller(from)} update link zoom bermasalah`,
            detail: `Detail: ${response.result}`,
            stack: null,
          },
        });
      } else {
        telegram.sendMessage(envConfig('GROUP_ID'), 'Informasi IFS‼ ⚠⚠⚠\nLINK ZOOM TELAH DIUPDATE', {
          parse_mode: 'Markdown',
        });
      }
    },
  },
  linkZoom: {
    description: 'Link zoom IFS hari ini',
    group: true,
    commandFrom: 'text',
    middlewares: ['date'],
    addToMyCommand: true,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      let response = await axios({
        url: envConfig('GSCRIPT_URL') + '?script=getZoomLink',
        method: 'get',
      });
      response = get(response, 'data.response');

      if (parseInt(response.code) != 200) {
        errorHandler(null, {
          logger: `Get zoom link error with status: ${response.result}`,
          rawOption: false,
        });
      }

      const zoomUrl = response.result;
      const msg =
        'Ini link zoomnya:\n' +
        `👉️ [KLIK DI SINI](${zoomUrl})` +
        '\n\n' +
        '⚠️ Jangan lupa ya untuk rename ketika di dalam zoom\n' +
        '👉️ ID Ingress - [RES/ENL]';

      telegram.sendMessage(envConfig('GROUP_ID'), `${userCaller(get(ctx, 'update.message.from'))}\n${msg}`, {
        parse_mode: 'Markdown',
        disable_web_page_preview: true,
      });
    },
  },
};
