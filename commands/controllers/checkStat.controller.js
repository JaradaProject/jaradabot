const { get } = require('lodash');
const { telegram } = require('../../helpers/bot.helper');
const errorHandler = require('../../helpers/error.helper');
const { ingressStatHelper } = require('../../helpers/ingressStat.helper');
const { userCaller, getCommandString, getArgument } = require('../../helpers/utils.helper');

module.exports = {
  checkStat: {
    description: 'Checking Stat Format',
    group: false,
    commandFrom: 'text',
    middlewares: ['owner'],
    addToMyCommand: false,
    // eslint-disable-next-line no-unused-vars
    controller: async (ctx, next) => {
      //** check format command */
      const { from, text } = get(ctx, 'update.message');
      const comm = getCommandString(text).command;
      const argument = await getArgument(comm, text).catch(() => {
        errorHandler(null, {
          logger: null,
          rawOption: false,
          data: {
            id: from.id,
            name: 'Masalah menggunakan command',
            message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
            detail: `Format submit stat seharusnya:\n${getCommandString(text).removeBotName()}<spasi>STATS INGRESS PRIME`,
            stack: null,
          },
        });
      });

      await ingressStatHelper(
        argument
          .split('\n')
          .map((t) => t.replace(/\s\s/g, ' '))
          .join('\n')
      );
      telegram.sendMessage(from.id, 'Stat checked and its okay', { parse_mode: 'Markdown' });
    },
  },
};
