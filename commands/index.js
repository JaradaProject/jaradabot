/* eslint-disable no-empty */
const fs = require('fs');
const Router = require('telegraf').Router;
const bot = require('../helpers/bot.helper');
const errorMiddleware = require('../middlewares/error.middleware');
const middlewares = require('../middlewares');
const { telegram } = require('../helpers/bot.helper');
const { envConfig, userCaller } = require('../helpers/utils.helper');
const groupMiddleware = require('../middlewares/group.middleware');

//** CONTROLLER LOADER */
const ctrl = {};
const controllersDir = fs.readdirSync('./commands/controllers');
controllersDir.forEach((file) => {
  if (file != 'index.js') {
    const data = require(`./controllers/${file}`);
    Object.keys(data).forEach((key) => {
      if (ctrl[key]) {
        throw new Error(`controller duplicate: ${key} from file: ${file}`);
      }
      ctrl[key] = data[key];
    });
  }
});

//** Telegram Command Build */
const listCommands = [];
Object.keys(ctrl).forEach((comm) => {
  const commandString = `/${comm.toLowerCase()}`;
  if (ctrl[comm].addToMyCommand) {
    listCommands.push({
      command: `${commandString}`,
      description: ctrl[comm].description,
    });
  }

  const obj = ctrl[comm];
  if (typeof obj.controller != 'function') {
    throw new Error(`Controller of ${obj.controller} Not Found`);
  }

  const routing = new Router(({ message }) => {
    try {
      return { route: message[`${obj.commandFrom}`].split(' ')[0] };
    } catch (error) {}
  });

  routing.on(`${commandString}`, async (ctx, next) => {
    try {
      await groupMiddleware(ctx, obj.group);
      await middlewares(ctx, obj.middlewares);
      await obj.controller.bind(ctrl)(ctx, next);
    } catch (error) {
      bot.use(errorMiddleware(error));
    }
  });

  routing.on(`${commandString}@${envConfig('BOT_USERNAME')}`, async (ctx, next) => {
    try {
      await groupMiddleware(ctx, obj.group);
      await middlewares(ctx, obj.middlewares);
      await obj.controller.bind(ctrl)(ctx, next);
    } catch (error) {
      bot.use(errorMiddleware(error));
    }
  });

  console.log('✔ Load [Command] ➡', commandString);
  bot.use(routing);
});

//** Telegram Welcome Route */
const welcomeRoute = new Router(({ message }) => {
  try {
    if (message.new_chat_member) return { route: 'new_chat_member' };
  } catch (error) {}
});

welcomeRoute.on('new_chat_member', (ctx) => {
  const newUser = ctx.update.message.new_chat_member;
  const msg = !newUser.username ? `Jangan lupa set username telegramnya ya biar aku enak manggilnya.` : '';
  ctx.replyWithMarkdown(`👋️ Selamat datang di group IFS XFACT ID,\n` + `Om/Tante ${userCaller(newUser)}\n` + `${msg}`);
});

bot.use(welcomeRoute);
telegram.setMyCommands(listCommands);
