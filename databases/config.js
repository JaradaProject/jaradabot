const { envConfig } = require('../helpers/utils.helper');

module.exports = {
  development: {
    username: envConfig('DB_USER'),
    password: envConfig('DB_PASS'),
    database: envConfig('DB_NAME'),
    host: envConfig('DB_HOST'),
    port: envConfig('DB_PORT'),
    dialect: 'mysql',
    logging: false,
    connectTimeout: 15000,
    pool: {
      max: 15,
      min: 5,
      idle: 20000,
      evict: 15000,
      acquire: 30000,
    },
  },
  production: {
    username: envConfig('DB_USER'),
    password: envConfig('DB_PASS'),
    database: envConfig('DB_NAME'),
    host: envConfig('DB_HOST'),
    port: envConfig('DB_PORT'),
    dialect: 'mysql',
    logging: false,
    connectTimeout: 15000,
    pool: {
      max: 15,
      min: 5,
      idle: 20000,
      evict: 15000,
      acquire: 30000,
    },
  },
};
