'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('rsvps', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      userId: {
        allowNull: false,
        type: Sequelize.STRING,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      eventId: {
        allowNull: false,
        type: Sequelize.STRING,
        references: {
          model: 'events',
          key: 'id',
        },
      },
      startStat: {
        allowNull: true,
        type: Sequelize.JSON,
      },
      endStat: {
        allowNull: true,
        type: Sequelize.JSON,
      },
      submitedStartStat: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.INTEGER,
      },
      submitedEndStat: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        defaultValue: new Date(),
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        defaultValue: new Date(),
        type: Sequelize.DATE,
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('rsvps');
  },
};
