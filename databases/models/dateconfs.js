'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class dateConfs extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
    }
  }
  dateConfs.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      comm: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      minDate: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      maxDate: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        defaultValue: new Date(),
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        defaultValue: new Date(),
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: 'dateConfs',
    }
  );
  return dateConfs;
};
