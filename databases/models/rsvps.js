'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rsvps extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.users);
      this.belongsTo(models.events);
    }
  }
  rsvps.init(
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.STRING,
      },
      userId: {
        allowNull: false,
        type: DataTypes.STRING,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      eventId: {
        allowNull: false,
        type: DataTypes.STRING,
        references: {
          model: 'events',
          key: 'id',
        },
      },
      startStat: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      endStat: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      submitedStartStat: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.INTEGER,
      },
      submitedEndStat: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        defaultValue: new Date(),
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        defaultValue: new Date(),
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: 'rsvps',
    }
  );
  return rsvps;
};
