const { get } = require('lodash');
const db = require('../databases/models');
const errorHandler = require('../helpers/error.helper');

module.exports = async (ctx) => {
  const id = get(ctx, 'update.message.from.id');
  const user = await db.users.findOne({
    raw: true,
    where: { id },
    attributes: ['typeUser'],
  });
  const typeUser = user == null ? 3 : user.typeUser;
  if (typeUser != 1) {
    ctx.deleteMessage(get(ctx, 'update.message.message_id')).catch(() => {});
    return errorHandler('UNSEND_ERR');
  }
};
