const errorHandler = require('../helpers/error.helper');
const db = require('../databases/models');
const { get } = require('lodash');
const { time, userCaller, getCommandString } = require('../helpers/utils.helper');

module.exports = async (ctx) => {
  const comm = getCommandString(get(ctx, 'update.message.text')).removeBotName();
  const id = get(ctx, 'update.message.chat.id');
  const from = get(ctx, 'update.message.from');

  const eventDate = await db.dateConfs
    .findOne({
      raw: true,
      where: { comm: 'event' },
      attributes: ['minDate', 'maxDate'],
    })
    .catch((error) => {
      errorHandler(error, {
        logger: 'Error mendapatkan tanggal event di database',
        rawOption: true,
        data: {
          id,
          name: 'Masalah menggunakan command',
          message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
          detail: `Ada permasalahan pada database, memanggil @SkyJackerz`,
          stack: true,
        },
      });
    });

  if (!eventDate) {
    errorHandler(null, {
      logger: 'Error mendapatkan tanggal event di database',
      rawOption: false,
      data: {
        id,
        name: 'Masalah menggunakan command',
        message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
        detail: `Ada permasalahan pada database, memanggil @SkyJackerz`,
        stack: true,
      },
    });
  }

  if (time(new Date()).isBefore(time(eventDate.minDate))) {
    errorHandler(null, {
      logger: null,
      rawOption: false,
      data: {
        id,
        name: 'Masalah menggunakan command',
        message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
        detail: `Command ${comm} hanya bisa digunakan saat event`,
        stack: false,
      },
    });
  }

  if (time(new Date()).isAfter(time(eventDate.maxDate))) {
    errorHandler(null, {
      logger: null,
      rawOption: false,
      data: {
        id,
        name: 'Masalah menggunakan command',
        message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
        detail: `Event telah lewat command ${comm} telah dinonaktifkan`,
        stack: false,
      },
    });
  }

  const commDate = await db.dateConfs
    .findOne({
      raw: true,
      where: { comm },
      attributes: ['minDate', 'maxDate'],
    })
    .catch((error) => {
      errorHandler(error, {
        logger: 'Error mendapatkan tanggal event di database',
        rawOption: true,
        data: {
          id,
          name: 'Masalah menggunakan command',
          message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
          detail: `Ada permasalahan pada database memanggil @SkyJackerz`,
          stack: true,
        },
      });
    });

  if (!commDate) {
    errorHandler(null, {
      logger: 'Error mendapatkan limitasi tanggal pada command di database',
      rawOption: false,
      data: {
        id,
        name: 'Masalah menggunakan command',
        message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
        detail: `Ada permasalahan pada database, memanggil @SkyJackerz`,
        stack: false,
      },
    });
  }

  if (time(new Date()).isBefore(time(commDate.minDate))) {
    errorHandler(null, {
      logger: null,
      rawOption: false,
      data: {
        id,
        name: 'Masalah menggunakan command',
        message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
        detail: `Command ${comm} hanya bisa digunakan setelah ${time(commDate.minDate).format('DD/MM/YYYY HH:mm')} WIB`,
        stack: null,
      },
    });
  }

  if (time(new Date()).isAfter(time(commDate.maxDate))) {
    errorHandler(null, {
      logger: null,
      rawOption: false,
      data: {
        id,
        name: 'Masalah menggunakan command',
        message: `Hai, ${userCaller(from)} Command tidak bisa digunakan`,
        detail: `Command ${comm} hanya bisa digunakan sebelum ${time(commDate.maxDate).format('DD/MM/YYYY HH:mm')} WIB`,
        stack: null,
      },
    });
  }
};
