const errorHandler = require('../helpers/error.helper');
const { get } = require('lodash');
const { envConfig } = require('../helpers/utils.helper');
module.exports = (ctx, isGroup) => {
  if (isGroup) {
    const update = get(ctx, 'update');
    const errCommand = update.message.text.split(' ')[0];
    const { id, type } = update.message.chat;
    if (type == 'private') {
      errorHandler(null, {
        logger: null,
        rawOption: false,
        data: {
          id,
          name: 'Masalah menggunakan command',
          message: 'Command tidak bisa digunakan, silahakan baca deskripsi untuk menggunakan command ini',
          detail: `Command ${errCommand} dikhususkan untuk digunakan dalam group`,
          stack: null,
        },
      });
    }
  } else {
    const update = get(ctx, 'update');
    const errCommand = update.message.text.split(' ')[0];
    const { id, type } = update.message.chat;
    if (type != 'private') {
      errorHandler(null, {
        logger: null,
        rawOption: false,
        data: {
          id,
          name: 'Masalah menggunakan command',
          message: 'Command tidak bisa digunakan, silahakan baca deskripsi untuk menggunakan command ini',
          detail: `Command ${errCommand} dikhususkan untuk digunakan dalam private chat dengan @${envConfig(
            'BOT_USERNAME'
          )}`,
          stack: null,
        },
      });
    }
  }
};
