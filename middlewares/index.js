const dateMiddleware = require('./date.middleware');
const ownerMiddleware = require('./owner.middleware');
const vipMiddleware = require('./vip.middleware');

module.exports = async (ctx, middlewares) => {
  if (middlewares.includes('date')) {
    await dateMiddleware(ctx);
  }
  if (middlewares.includes('vip')) {
    await vipMiddleware(ctx);
  }
  if (middlewares.includes('owner')) {
    await ownerMiddleware(ctx);
  }
};
