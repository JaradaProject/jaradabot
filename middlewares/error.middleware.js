const telegram = require('../helpers/bot.helper').telegram;

module.exports = (err) => {
  if (err.sendTo) {
    const msg = `‼️ ${err.name} ‼️\n${err.message}\n\n⚠️ Detail masalah:\n${err.detail}`;
    telegram.sendMessage(err.sendTo, msg, {
      parse_mode: 'Markdown',
    });
    return;
  }
};
