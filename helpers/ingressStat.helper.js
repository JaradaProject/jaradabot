const { isObject } = require('lodash');
const errorHandler = require('./error.helper');
const ingressPrimeStatsToJson = require('./ingressStatsParser');
const { envConfig } = require('./utils.helper');

module.exports = {
  ingressStatHelper: async (stats) => {
    try {
      if (isObject(stats)) {
        const statKeys = Object.keys(stats);
        statKeys.push('\n');
        Object.keys(stats).forEach((key) => {
          statKeys.push(stats[key]);
        });
        return {
          json: stats,
          formated: statKeys.join('        ').split('\n        ').join('\n'),
        };
      }
      const statJson = await ingressPrimeStatsToJson(stats);
      if (statJson['Time Span'] != 'ALL TIME') {
        errorHandler(null, {
          logger: `Stats error`,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah Ingress Stats',
            message: `Hai, Ada masalah saat parsing Ingress Stats`,
            detail: 'Format stats Ingress kamu seharusnya yang ALL TIME, bingung hubungi @SkyJackerz',
            stack: null,
          },
        });
      }

      const statKeys = Object.keys(statJson);
      statKeys.push('\n');
      Object.keys(statJson).forEach((key) => {
        statKeys.push(statJson[key]);
      });

      return {
        json: statJson,
        formated: statKeys.join('        ').split('\n        ').join('\n'),
      };
    } catch (error) {
      if (error.message.includes('Expect the string to have two lines')) {
        errorHandler(null, {
          logger: `Some one have error in stat`,
          rawOption: false,
          data: {
            id: envConfig('GROUP_ID'),
            name: 'Masalah Ingress Stats',
            message: `Hai, Ada masalah saat parsing Ingress Stats`,
            detail:
              'Format stats Ingress kamu ada yang salah, detail masalah "Stats harus dua baris", Tolong hubungi @SkyJackerz',
            stack: null,
          },
        });
      }
      if (error.message.includes('unknown key was added')) {
        errorHandler(error, {
          logger: `Stats error`,
          rawOption: true,
          data: {
            id: envConfig('OWNER_ID'),
            name: 'Masalah Ingress Stats',
            message: `Hai, Ada masalah saat parsing Ingress Stats`,
            detail: 'Stat Ingress berbeda dari yang Jarada ketahui, Tolong hubungi @SkyJackerz',
            stack: null,
          },
        });
      } else {
        errorHandler(error, {
          logger: `Stats error`,
          rawOption: true,
          data: {
            id: envConfig('OWNER_ID'),
            name: 'Masalah Ingress Stats',
            message: `Hai, Ada masalah saat parsing Ingress Stats`,
            detail: 'Format stats Ingress kamu ada yang salah, Tolong hubungi @SkyJackerz',
            stack: null,
          },
        });
      }
    }
  },
};
