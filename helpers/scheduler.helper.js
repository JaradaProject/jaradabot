const schedule = require('node-schedule');
const { time } = require('./utils.helper');

module.exports = {
  /**
   *
   * @param {string} schedulerName - Name of scheduler
   * @param {string} startDateTime - Date time format when the schedule need to start
   * @param {string} endDateTime - Date time format when the schedule need to end
   * @param {number} interval - The interval of the scheduler, 1 = 1 second
   * @param {function} runningFunction - The function that must be running
   */
  task: ({ schedulerName, startDateTime, endDateTime, interval, runningFunction }) => {
    if (time(endDateTime).isBefore(time())) {
      console.log(`❌ Load [Scheduler Canceled] : Error parsing end date on ${schedulerName}`);
      return;
    }
    const listDate = [time(startDateTime).toDate()];
    // eslint-disable-next-line no-constant-condition
    while (true) {
      const date = listDate.slice(-1).pop();
      const newDate = time(date).add(interval, 'second');
      if (time(newDate).isBefore(time(endDateTime))) listDate.push(time(newDate).toDate());
      else break;
    }

    listDate.forEach((date) => {
      schedule.scheduleJob(date, async () => {
        console.log('[Sceduler - Task Run]:', schedulerName);
        await runningFunction();
      });
    });
  },
  /**
   *
   * @param {string} schedulerName - Name of scheduler
   * @param {string} dateTimeTrigger - Date time format when the schedule need to trigger
   * @param {function} runningFunction - The function that must be running
   */
  trigger: ({ schedulerName, dateTimeTrigger, runningFunction }) => {
    if (time(dateTimeTrigger).isBefore(time())) {
      console.log(`❌ Load [Scheduler Canceled] : Error parsing date time on ${schedulerName}`);
      return;
    } else {
      schedule.scheduleJob(time(dateTimeTrigger).toDate(), async () => {
        console.log('[Sceduler - Task Run]:', schedulerName);
        await runningFunction();
      });
    }
  },
};
