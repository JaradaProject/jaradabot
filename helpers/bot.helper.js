const { Telegraf } = require('telegraf');
const TelegrafLogger = require('telegraf-logger');
const { envConfig } = require('./utils.helper');

const bot = new Telegraf(envConfig('BOT_TOKEN'));

const logger = new TelegrafLogger({
  log: console.log,
  format: '%ut => @%u %fn %ln (%fi): <%ust> %c',
  contentLength: 100,
});

if (envConfig('NODE_ENV') == 'development') {
  bot.use(logger.middleware());
}

module.exports = bot;
