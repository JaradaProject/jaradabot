const axios = require('axios');
const cheerio = require('cheerio');
const { envConfig } = require('./utils.helper');
const db = require('../databases/models');

module.exports = {
  eventChecker: async () => {
    const event = await db.events.findOne({ raw: true, where: { status: 'active' }, attributes: ['id'] });
    if (event == null) {
      console.log('');
      return;
    }
    const urlEvent = envConfig('FEVGAMES_EVENT_URL');
    const html = await axios.get(`${urlEvent}${event.id}`);
    const $ = cheerio.load(html.data);

    const rsvpPlayers = [];
    function collector(e) {
      if ($(e)[0].type == 'text') {
        const text = $(e)
          .text()
          .match(/([A*-z*])\w+/g);
        if (text != null) rsvpPlayers.push(text[0]);
      }
    }

    $('div#Res')['0'].children.forEach((element) => {
      collector(element);
    });
    $('div#Enl')['0'].children.forEach((element) => {
      collector(element);
    });
    return { event, rsvpPlayers };
  },
};
