class CustomError extends Error {
  constructor(sendTo, name, message, detail, stack) {
    super();
    this.sendTo = sendTo;
    this.name = name;
    this.message = message;
    this.detail = detail;
    this.stack = stack;
  }
}

/**
 *
 * @err {Error}
 * @options {Object: {logger: string, rawOption: true | false, data: {id: string, name: string, message: string, detail: string, stack: string}}}
 */
function errorHandler(err, options) {
  if (err == 'UNSEND_ERR') {
    throw new CustomError(false);
  }
  if (err != null) {
    if (options.rawOption) {
      console.error(`${options.logger} Raw Error: \n`, err);
    }
    console.error(`${options.logger}: \n`, JSON.stringify(err));
  } else {
    if (options.logger != null) {
      console.error(`${options.logger}`);
    }
  }

  if (options.data.stack) {
    throw new CustomError(options.data.id, options.data.name, options.data.message, options.data.detail);
  } else {
    throw new CustomError(options.data.id, options.data.name, options.data.message, options.data.detail, null);
  }
}

module.exports = errorHandler;
