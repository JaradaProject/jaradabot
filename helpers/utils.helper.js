require('dotenv').config();
const { nanoid, customAlphabet } = require('nanoid');
const time = require('dayjs');
const customParseFormat = require('dayjs/plugin/customParseFormat');
const errorHandler = require('./error.helper');

time.locale('id');
time.extend(customParseFormat);

const utils = {
  time,
  envConfig: (envKey) => {
    const devProdEnvList = {
      BOT_USERNAME: {
        production: 'BOT_NAME_PROD',
        development: 'BOT_NAME_DEV',
      },
      BOT_TOKEN: {
        production: 'BOT_TOKEN_PROD',
        development: 'BOT_TOKEN_DEV',
      },
      GROUP_ID: {
        production: 'GROUP_ID_PROD',
        development: 'GROUP_ID_DEV',
      },
      DB_USER: {
        production: 'DB_USER_PROD',
        development: 'DB_USER_DEV',
      },
      DB_PASS: {
        production: 'DB_PASS_PROD',
        development: 'DB_PASS_DEV',
      },
      DB_NAME: {
        production: 'DB_NAME_PROD',
        development: 'DB_NAME_DEV',
      },
      DB_HOST: {
        production: 'DB_HOST_PROD',
        development: 'DB_HOST_DEV',
      },
      DB_PORT: {
        production: 'DB_PORT_PROD',
        development: 'DB_PORT_DEV',
      },
    };
    const check = (envKey) => {
      // eslint-disable-next-line no-undef
      if (process.env[envKey]) {
        // eslint-disable-next-line no-undef
        return process.env[envKey];
      } else {
        throw new Error(`${envKey} isn't declared in .env file. Please double check it!`);
      }
    };
    if (Object.keys(devProdEnvList).includes(envKey)) {
      return check(devProdEnvList[envKey][check('NODE_ENV')]);
    }
    return check(envKey);
  },
  userCaller: (object) => {
    if (!object.username) {
      let fullName = `${object.first_name}`;
      if (object.last_name) fullName = `${object.first_name} ${object.last_name}`;
      return `[${fullName}](tg://user?id=${object.id})`;
    }
    return `[${object.username}](tg://user?id=${object.id})`;
  },
  getCommandString: (text) => {
    let command = text.split(' ')[0];
    return {
      command,
      removeBotName: () => {
        return command.includes('@') ? command.split('@')[0] : command;
      },
    };
  },
  getArgument: async (comm, text) => {
    const argument = text.replace(`${comm} `, '');
    if (argument == comm || argument == '') {
      throw new Error('Argument needed');
    }
    return argument;
  },
  idGenerator: {
    default: async (length) => {
      length = parseInt(length);
      return await nanoid(length);
    },
    caps: async (length) => {
      length = parseInt(length);
      return await customAlphabet('ABCDEFGHIJKLMNOPQRSTUVWXYZ', length)();
    },
    capsNumber: async (length) => {
      length = parseInt(length);
      return await customAlphabet('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890', length)();
    },
    lower: async (length) => {
      length = parseInt(length);
      return await customAlphabet('abcdefghijklmnopqrstuvwxyz', length)();
    },
    lowerNumber: async (length) => {
      length = parseInt(length);
      return await customAlphabet('abcdefghijklmnopqrstuvwxyz1234567890', length)();
    },
    number: async (length) => {
      length = parseInt(length);
      return await customAlphabet('1234567890', length)();
    },
    lowerCapsNumber: async (length) => {
      length = parseInt(length);
      return await customAlphabet('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890', length)();
    },
  },
  deleteMessage: (ctx, message_id) => {
    try {
      ctx.deleteMessage(message_id);
    } catch (error) {
      errorHandler(error, {
        logger: `Delete message_id ${message_id} failed`,
        rawOption: false,
        data: {
          id: null,
          name: null,
          message: null,
          detail: null,
          stack: null,
        },
      });
    }
  },
  numberFormat: new Intl.NumberFormat('id-ID'),
};

module.exports = utils;
