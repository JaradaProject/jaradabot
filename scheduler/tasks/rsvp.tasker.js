const db = require('../../databases/models');
const { envConfig, userCaller, idGenerator } = require('../../helpers/utils.helper');
const { isNull } = require('lodash');
const { telegram } = require('../../helpers/bot.helper');
const { eventChecker } = require('../../helpers/fevgames.helper');

module.exports = {
  autoRsvp: {
    startDateTime: '2022-06-01 00:00:00',
    endDateTime: '2022-06-04 06:00:00',
    interval: 300,
    runningFunction: async () => {
      const { event, rsvpPlayers } = await eventChecker();

      //** Get user have saved before */
      const users = await db.users.findAll({ raw: true });

      const rsvps = [];
      const unknown = [];

      await Promise.all(
        rsvpPlayers.map(async (name) => {
          const isUser = users.find((user) => user.name == name);
          if (isUser) {
            const userId = isUser.id;
            const isRsvp = await db.rsvps.findOne({ raw: true, where: { userId, eventId: event.id } });
            if (isNull(isRsvp)) {
              await db.rsvps
                .create({ id: await idGenerator.capsNumber(32), userId, eventId: event.id, startStat: {}, endStat: {} })
                .catch(() => {});
              console.log(`[RSVP] :`, name);
              rsvps.push(userCaller({ id: userId, username: name }));
            }
          } else {
            const isUnknown = await db.unknowns.findOne({ raw: true, where: { name } });
            if (isNull(isUnknown)) {
              await db.unknowns.create({ name, eventId: event.id }).catch(() => {});
              console.log(`[Unkonwn RSVP] :`, name);
              unknown.push(`@${name}`);
            }
          }
        })
      );

      const urlEvent = envConfig('FEVGAMES_EVENT_URL');
      let msg;
      if (rsvps.length == 0 && unknown.length == 0) msg = '';
      if (rsvps.length > 0 && unknown.length == 0) {
        msg =
          `${rsvps.join(', ')}\n` +
          `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
          `Nanti saat event dimulai, Jarada bakal ingetin untuk update stat.\n\n` +
          `Bagi yang mau RSVP bisa ke: ${urlEvent}${event.id}`;
      }
      if (rsvps.length == 0 && unknown.length > 0) {
        msg =
          `${unknown.join(', ')}\n` +
          `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
          `Apa bila ada yang kenal mohon bantuan untuk mengajak bergabung ke grup ini.\n` +
          `Apabila sudah ada di grup ini maka check ulang ID yang didaftarkan ke Fevgames terlebih dahulu.\n` +
          `Apabila sebelumnya belum pernah ikut IFS silahkan rsvp manual.\n` +
          `Gunakan: /rsvp<spasi>STATS INGRESS KAMU.\n` +
          `Dan bila sudah, Jarada ucapkan selamat datang.\n\n` +
          `Bagi yang mau RSVP bisa ke: ${urlEvent}${event.id}`;
      }
      if (rsvps.length > 0 && unknown.length > 0) {
        msg =
          `Halo ` +
          `${rsvps.join(', ')}\n` +
          `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
          `Nanti saat event dimulai, Jarada bakal ingetin untuk update stat.\n\n` +
          `Dan buat,\n` +
          `${unknown.join('\n')}\n` +
          `Juga terpantau sudah RSVP di Fevgames` +
          `Apa bila ada yang kenal mohon bantuan untuk mengajak bergabung ke grup ini.\n` +
          `Apabila sudah ada di grup ini maka check ulang ID yang didaftarkan ke Fevgames terlebih dahulu.\n` +
          `Apabila sebelumnya belum pernah ikut IFS silahkan rsvp manual.\n` +
          `Gunakan: /rsvp<spasi>STATS INGRESS KAMU.\n` +
          `Dan bila sudah, Jarada ucapkan selamat datang.\n\n` +
          `Bagi yang mau RSVP bisa ke: ${urlEvent}${event.id}`;
      }

      if (msg == '') return;
      telegram.sendMessage(envConfig('GROUP_ID'), msg, { parse_mode: 'Markdown', disable_web_page_preview: true });
    },
  },
};
