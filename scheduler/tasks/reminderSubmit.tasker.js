const db = require('../../databases/models');
const { userCaller, time, envConfig } = require('../../helpers/utils.helper');
const { telegram } = require('../../helpers/bot.helper');
// const { Op } = require('sequelize');
// const { isNull } = require('lodash');

module.exports = {
  reminderStartStat: {
    startDateTime: '2022-06-04 08:00:00',
    endDateTime: '2022-06-04 14:30:00',
    interval: 900,
    runningFunction: async () => {
      const event = await db.events.findOne({ raw: true, where: { status: 'active' }, attributes: ['id'] });

      let rsvps = await db.rsvps.findAll({ raw: true, where: { eventId: event.id } });
      rsvps = rsvps.filter((i) => Object.keys(i.startStat) == 0 || i.startStat == null);
      let users = await Promise.all(
        rsvps.map(async (r) => {
          return await db.users.findOne({ raw: true, where: { id: r.userId }, attributes: ['id', 'name'] });
        })
      );
      if (!users.length) {
        console.log('[Tasker - submitStartStat] : All rsvp has submit end stat');
        return;
      }
      users = users
        .map((user) => {
          return userCaller({ id: user.id, username: user.name });
        })
        .join(', ');

      let { maxDate } = await db.dateConfs.findOne({ raw: true, where: { comm: '/statawal' }, attributes: ['maxDate'] });
      maxDate = time(maxDate).format('HH:mm');

      const msg =
        '⚠⚠⚠ ALERT REMINDER ⚠⚠⚠\n\n' +
        `Halo 👋👋👋\n${users}\n\n` +
        'Jangan lupa melakukan submit stat awal ya!!!\n' +
        'Untuk stat awal gunakan command /statawal\n\n' +
        'Stat awal bisa dilakukan sampai pukul:\n' +
        `${maxDate} WIB. Terima Kasih.`;

      telegram.sendMessage(envConfig('GROUP_ID'), msg, { parse_mode: 'Markdown' });
    },
  },
  reminderEndStat: {
    startDateTime: '2022-06-04 12:00:00',
    endDateTime: '2022-06-04 20:00:00',
    interval: 900,
    runningFunction: async () => {
      const event = await db.events.findOne({ raw: true, where: { status: 'active' }, attributes: ['id'] });

      let rsvps = await db.rsvps.findAll({ raw: true, where: { eventId: event.id } });
      rsvps = rsvps.filter((i) => Object.keys(i.startStat).length > 0 || i.startStat != null);
      rsvps = rsvps.filter((i) => Object.keys(i.endStat).length == 0 || i.endStat == null);
      let users = await Promise.all(
        rsvps.map(async (r) => {
          return await db.users.findOne({ raw: true, where: { id: r.userId }, attributes: ['id', 'name'] });
        })
      );
      if (!users.length) {
        console.log('[Tasker - submitStartStat] : All rsvp has submit end stat');
        return;
      }
      users = users
        .map((user) => {
          return userCaller({ id: user.id, username: user.name });
        })
        .join(', ');

      let { maxDate } = await db.dateConfs.findOne({ raw: true, where: { comm: '/statakhir' }, attributes: ['maxDate'] });
      maxDate = time(maxDate).format('HH:mm');

      const msg =
        '⚠⚠⚠ ALERT REMINDER ⚠⚠⚠\n\n' +
        `Halo 👋👋👋\n${users}\n\n` +
        'Jangan lupa melakukan submit stat akhir ya!!!\n' +
        'Untuk stat akhir gunakan command /statakhir\n\n' +
        'Stat akhir bisa dilakukan sampai pukul:\n' +
        `${maxDate} WIB. Terima Kasih.`;

      telegram.sendMessage(envConfig('GROUP_ID'), msg, { parse_mode: 'Markdown' });
    },
  },
};
