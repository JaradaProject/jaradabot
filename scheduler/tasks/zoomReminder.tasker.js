const db = require('../../databases/models');
const { telegram } = require('../../helpers/bot.helper');
const { envConfig, userCaller } = require('../../helpers/utils.helper');

module.exports = {
  zoomReminder: {
    startDateTime: '2022-06-04 12:50:00',
    endDateTime: '2022-06-04 15:00:00',
    interval: 3600,
    runningFunction: async () => {
      const msg1 =
        '⚠⚠⚠ ALERT REMINDER ⚠⚠⚠\n\n' +
        `Halo 👋👋👋\n\n` +
        'Jangan lupa ikut sesi zoom ya!!!\n' +
        '10 Menit lagi akan dilakukan sesi foto\n' +
        'ROOM AKAN DIKUNCI SEWAKTU WAKTU!!!!';
      telegram.sendMessage(envConfig('GROUP_ID'), msg1, { parse_mode: 'Markdown' });

      const event = await db.events.findOne({ raw: true, where: { status: 'active' }, attributes: ['id'] });
      let rsvps = await db.rsvps.findAll({
        raw: true,
        where: { eventId: event.id },
        attributes: [],
        includes: {
          model: db.users,
          attributes: ['id', 'name'],
        },
      });
      rsvps = rsvps
        .map((r) => {
          return userCaller({ id: r['user.id'], username: r['user.name'] });
        })
        .join(', ');

      const msg2 = `⚠⚠⚠ ALERT REMINDER ⚠⚠⚠\n\n Halo 👋👋👋\n\n ${rsvps}\n\n AYO IKU ZOOM ketik /linkzoom untuk masuk`;
      telegram.sendMessage(envConfig('GROUP_ID'), msg2, { parse_mode: 'Markdown' });
    },
  },
};
