const db = require('../../databases/models');
const { userCaller, envConfig } = require('../../helpers/utils.helper');
const { telegram } = require('../../helpers/bot.helper');
const { eventChecker } = require('../../helpers/fevgames.helper');

module.exports = {
  autoUnRsvp: {
    startDateTime: '2022-06-01 00:00:00',
    endDateTime: '2022-06-04 06:00:00',
    interval: 300,
    runningFunction: async () => {
      let { event, rsvpPlayers } = await eventChecker();
      let unRsvp = [];
      const rsvps = await db.users.findAll({ raw: true, include: { model: db.rsvps, where: { eventId: event.id } } });
      const unknown = await db.unknowns.findAll({ raw: true, where: { eventId: event.id } });

      rsvps.forEach((rsvp) => {
        if (['340704057', '273743485'].includes(rsvp.id)) {
          return;
        }
        if (!rsvpPlayers.includes(rsvp.name)) {
          console.log(rsvp.name);
          unRsvp.push({
            id: rsvp.id,
            name: rsvp.name,
          });
        }
      });
      unknown.forEach((u) => {
        if (!rsvpPlayers.includes(u.name)) {
          unRsvp.push({
            id: null,
            name: u.name,
          });
        }
      });

      const unRsvpPlayer = unRsvp.map(async (player) => {
        if (player.id) {
          await db.rsvps.destroy({ where: { userId: player.id, eventId: event.id } });
          return userCaller({ id: player.id, username: player.name });
        } else {
          await db.unknowns.destroy({ where: { name: player.name, eventId: event.id } });
          return `@${player.name}`;
        }
      });

      Promise.all(unRsvpPlayer).then((listPlayer) => {
        if (listPlayer.length == 0) {
          return;
        }
        const msg =
          `Agent: ${listPlayer.join(', ')}\n` + `mengundurkan diri dari event kali ini. Sampai jumpa di event berikutnya.`;
        telegram.sendMessage(envConfig('GROUP_ID'), msg, { parse_mode: 'Markdown' });
      });
    },
  },
};
