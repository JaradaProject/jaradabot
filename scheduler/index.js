const schedulerHelper = require('../helpers/scheduler.helper');
const fs = require('fs');

module.exports = () => {
  const obj = {
    task: {},
    trigger: {},
  };

  const loader = (folder, listFile) => {
    listFile.forEach((file) => {
      const data = require(`./${folder}s/${file}`);
      Object.keys(data).forEach((key) => {
        if (obj[folder][key]) {
          throw new Error(`controller duplicate: ${key} from file: ${file}`);
        }
        if (!data[key].date) {
          obj[folder][key] = {
            schedulerName: key,
            ...data[key],
          };
        } else {
          obj[folder][key] = {
            schedulerName: key,
            ...data[key],
          };
        }
      });
    });
  };

  loader('task', fs.readdirSync('./scheduler/tasks'));
  loader('trigger', fs.readdirSync('./scheduler/triggers'));

  Object.keys(obj).forEach((typeScheduler) => {
    Object.keys(obj[typeScheduler]).forEach((s) => {
      console.log('✔ Load [Scheduler] ➡', s);
      schedulerHelper[typeScheduler](obj[typeScheduler][s]);
    });
  });
};
