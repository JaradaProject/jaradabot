const db = require('../../databases/models');
const axios = require('axios');
const { get } = require('lodash');
const { ingressStatHelper } = require('../../helpers/ingressStat.helper');
const { envConfig } = require('../../helpers/utils.helper');

const axiosRetry = require('axios-retry');
axiosRetry(axios, {
  retries: 5,
  retryDelay: (retryCount) => {
    return retryCount * 2000;
  },
});

module.exports = {
  submitStat: {
    dateTimeTrigger: '2022-06-04 20:05:00',
    runningFunction: async () => {
      const event = await db.events.findOne({ raw: true, where: { status: 'active' }, attributes: ['id'] });
      let stats = await db.rsvps.findAll({
        raw: true,
        where: { eventId: event.id },
        attributes: ['id', 'startStat', 'endStat'],
      });

      stats = stats.filter((s) => s.startStat != null && Object.keys(s.startStat).length > 0);
      const delayIncrement = 2000;
      let delay = 0;

      await Promise.all(
        stats.map(async (stat, index) => {
          delay += delayIncrement;
          return new Promise((resolve) => setTimeout(resolve, delay)).then(async () => {
            const requestedRow = index + 3;
            const start = await (await ingressStatHelper(stat.startStat)).formated;
            const end =
              stat.end != null || Object.keys(stat.endStat).length > 0
                ? await (
                    await ingressStatHelper(stat.endStat)
                  ).formated
                : '';
            const startResponse = await axios({
              url: envConfig('GSCRIPT_URL'),
              method: 'POST',
              params: {
                mode: 'production',
                script: 'submitAllStats',
              },
              data: {
                requestedRow,
                start,
                end,
              },
            });
            if (get(startResponse, 'data.response.code') == 200) {
              await db.rsvps
                .update({ submitedStartStat: requestedRow, submitedEndStat: true }, { where: { id: stat.id } })
                .then(() => {
                  console.log('[Tasker - submitStat] : Success submit for id ', stat.startStat['Agent Name']);
                });
            } else {
              console.log('[Tasker - submitStat] : Error submit for id ', stat.startStat['Agent Name']);
            }
          });
        })
      );
    },
  },
};
